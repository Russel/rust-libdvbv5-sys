/*
 *  libdvbv5-sys — a Rust FFI binding to the libdvbv5 library from V4L2.
 *
 *  Copyright © 2019, 2020  Russel Winder
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// Do not enforce linking to libdvbv5 when building the documentation on https://docs.rs

#[cfg(not(feature = "docs-rs"))]
use pkg_config;

#[cfg(feature = "docs-rs")]
fn main() {} // Skip the script when the doc is building

#[cfg(not(feature = "docs-rs"))]
fn main() {
    // Bionic on Travis-CI has 1.14.2, Buster on GitLab has 1.16.3.
    // pkg_config::Config::new().atleast_version("1.16").probe("libdvbv5").unwrap();
    pkg_config::Config::new().probe("libdvbv5").unwrap();
}

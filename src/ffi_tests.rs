/*
 *  libdvbv5-sys — a Rust FFI binding for the libdvbv5 library from V4L2.
 *
 *  Copyright © 2019, 2020  Russel Winder
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// Some tests to show that the FFI binding works.
//
// Can only easily check functions that do not require access to an adaptor.

#[cfg(test)]
mod tests {

    use std::ffi::CString;

    use crate::ffi;

    #[test]
    fn can_parse_delsys() {
        // TODO ffi::dvb_parse_delsys should return ffi::fe_delivery_system not ::std::os::raw::c_int
        assert_eq!(
            unsafe { ffi::dvb_parse_delsys(CString::new("DVB-T2").unwrap().as_ptr()) },
            ffi::fe_delivery_system::SYS_DVBT2 as i32
        );
    }

    #[test]
    fn can_parse_format() {
        assert_eq!(
            unsafe { ffi::dvb_parse_format(CString::new("DVBv5").unwrap().as_ptr()) },
            ffi::dvb_file_formats::FILE_DVBV5
        );
    }
}

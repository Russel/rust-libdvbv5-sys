/*
 *  libdvbv5-sys — a Rust FFI binding for the libdvbv5 library from V4L2.
 *
 *  Copyright © 2019, 2020  Russel Winder
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

//! This crate provides a Rust binding to the C API of the
//! [libdvbv5 library](https://linuxtv.org/docs/libdvbv5/) that is part of the
//! [V4L2 project](https://linuxtv.org/wiki/index.php/V4l-utils) that is part of the
//! [Linux TV](https://linuxtv.org/) effort.
//!
//! Linux has kernel level support for DVB devices. Working with them using the
//! system calls required is not easy. libdvbv5 provides an abstraction/middleware
//! layer over the kernel support to make working with DVB devices much easier.
//! The library is though focused on providing support for C programmers.
//!
//! This crate provides the Rust FFI to the C API of libdvbv5. See the
//! [dvbv5 crate](https://gitlab.com/Russel/rust-libdvbv5)
//! for various abstractions over the FFI to support Rust programmers.
//!
//! The binding is automatically generated using
//! [Bindgen](https://rust-lang.github.io/rust-bindgen/) so there isn't that much
//! in the way of detailed Rust focused documentation. There is always the
//! [C API documentation](https://linuxtv.org/docs/libdvbv5/) of course, but remember
//! (obviously) it is very C focused!

#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

mod ffi;
pub use ffi::*;

mod dvb_file_free;
pub use dvb_file_free::*;

mod debug;
pub use debug::*;

mod ffi_tests;
